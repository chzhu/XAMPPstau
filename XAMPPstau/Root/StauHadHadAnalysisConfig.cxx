#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPstau/StauHadHadAnalysisConfig.h>

namespace XAMPP {
    StauHadHadAnalysisConfig::StauHadHadAnalysisConfig(std::string Analysis) : AnalysisConfig(Analysis) {
        declareProperty("doPFlowCleaning", m_doPFlowCleaning = false);
    }

    StatusCode StauHadHadAnalysisConfig::initializeCustomCuts() {
        //########################################################################
        //          Had-had stream
        //########################################################################
        CutFlow HadHad("tautau");

        Cut* PFlowCleaning = NewSkimmingCut("PFlow Electron veto ", Cut::CutType::CutInt);
        if (m_doPFlowCleaning) {
            if (!PFlowCleaning->initialize("DFCommonCut", "==1")) return StatusCode::FAILURE;
            HadHad.push_back(PFlowCleaning);
        }

        Cut* Least1BaselineTauCut = NewSkimmingCut("N_{#tau}^{baseline}>=1", Cut::CutType::CutInt);
        if (!Least1BaselineTauCut->initialize("n_BaseTau", ">=1")) return StatusCode::FAILURE;
        HadHad.push_back(Least1BaselineTauCut);

        Cut* LeadTauPtCut = NewSkimmingCut("p_{T} (#tau^{0}) > 50 GeV", Cut::CutType::CutPartFloat);
        if (!LeadTauPtCut->initialize("taus pt[0]", ">=50000")) return StatusCode::FAILURE;
        HadHad.push_back(LeadTauPtCut);

        Cut* Least2BaselineTauCut = NewSkimmingCut("N_{#tau}^{baseline}>=2", Cut::CutType::CutInt);
        if (!Least2BaselineTauCut->initialize("n_BaseTau", ">=2")) return StatusCode::FAILURE;
        // HadHad.push_back(Least2BaselineTauCut);

        Cut* Least1BaselineMuon = NewSkimmingCut("N_{#mu}^{baseline} >= 1", Cut::CutType::CutInt);
        if (!Least1BaselineMuon->initialize("n_BaseMuon", ">=0")) return StatusCode::FAILURE;

        HadHad.push_back(Least1BaselineMuon->combine(Least2BaselineTauCut, Cut::OR));

        Cut* Least1SignalTauCut = NewCutFlowCut("N_{#tau}^{signal} >= 1", Cut::CutType::CutInt);
        if (!Least1SignalTauCut->initialize("n_SignalTau", ">=1")) return StatusCode::FAILURE;
        HadHad.push_back(Least1SignalTauCut);

        Cut* Exact2SignalTauCut = NewCutFlowCut("N_{#tau}^{signal} == 2", Cut::CutType::CutInt);
        if (!Exact2SignalTauCut->initialize("n_SignalTau", "==2")) return StatusCode::FAILURE;
        HadHad.push_back(Exact2SignalTauCut);

        Cut* OsTauTau = NewCutFlowCut("#tau^{+}#tau^{-}", Cut::CutType::CutChar);
        if (!OsTauTau->initialize("OS_TauTau", "==1")) return StatusCode::FAILURE;
        HadHad.push_back(OsTauTau);

        Cut* TrigCut = NewCutFlowCut("TrigMatching", Cut::CutType::CutChar);
        if (!TrigCut->initialize("TrigMatching", "=1")) return StatusCode::FAILURE;
        HadHad.push_back(TrigCut);

        // Veto Additional leptons ? -- let's leave it to be optimized
        Cut* BaselineElecVeto = NewCutFlowCut("N_{e}^{baseline} = 0", Cut::CutType::CutInt);
        if (!BaselineElecVeto->initialize("n_BaseElec", "=0")) return StatusCode::FAILURE;

        Cut* BaselineMuonVeto = NewCutFlowCut("N_{#mu}^{baseline} = 0", Cut::CutType::CutInt);
        if (!BaselineMuonVeto->initialize("n_BaseMuon", "=0")) return StatusCode::FAILURE;

        HadHad.push_back(BaselineMuonVeto->combine(BaselineElecVeto, Cut::AND));

        Cut* BVeto = NewCutFlowCut("B-veto", Cut::CutType::CutInt);
        if (!BVeto->initialize("n_BJets", "==0")) return StatusCode::FAILURE;
        HadHad.push_back(BVeto);

        ATH_CHECK(AddToCutFlows(HadHad));

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
