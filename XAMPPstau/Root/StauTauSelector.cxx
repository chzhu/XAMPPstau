#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <TauAnalysisTools/ITauSelectionTool.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/EventInfo.h>
#include <XAMPPbase/IJetSelector.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPbase/SUSYTauSelector.h>
#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauTauSelector.h>
#include <xAODTau/TauTrack.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include <fstream>
#include <iostream>
#include <sstream>

namespace XAMPP {

    const static FloatAccessor acc_width("Width");
    const static FloatDecorator dec_width("Width");
    const static FloatDecorator dec_trk_width("TrkJet_Width");
    const static IntDecorator dec_NTrksJet("NTrksJet");
    const static CharDecorator dec_jetAssoc("jetAssoc");
    const static IntDecorator dec_quality("Quality");
    const static CharDecorator dec_isPromoted("isPromoted");
    const static IntDecorator dec_NTrks("NTrks");

    const static FloatDecorator dec_z0sinTheta("z0sinTheta");
    const static FloatDecorator dec_d0sig("d0sig");
    const static FloatDecorator dec_d0("d0");

    StauTauSelector::StauTauSelector(const std::string& myname) :
        SUSYTauSelector(myname),
        m_jet_selection("SUSYJetSelector"),
        m_NumTauCandidate_OR(-1),
        m_performTauPromotion(false),
        m_performFakeEff(false),
        m_promotion_dsids(),
        m_random_seed(12345),
        m_promotion_before_OR(false),
        m_rnd_generator(),
        m_NumPoolPromotedTaus(nullptr),
        m_NumFakeTaus(nullptr),
        m_NumAllFakeTaus(nullptr),
        m_FlagPromotedEvent(nullptr) {
        declareProperty("TauCandidatesForOR", m_NumTauCandidate_OR);
        declareProperty("JetSelectionTool", m_jet_selection);
        declareProperty("doTauPromotion", m_performTauPromotion);
        declareProperty("doFakeEff", m_performFakeEff);
        declareProperty("PromotionSeed", m_random_seed);
        declareProperty("doPromotionOnlyFor", m_promotion_dsids);
        declareProperty("promoteTausBeforeOR", m_promotion_before_OR);
    }
    StauTauSelector::~StauTauSelector() {}

    StatusCode StauTauSelector::initialize() {
        if (isInitialized()) return StatusCode::SUCCESS;
        ATH_CHECK(SUSYTauSelector::initialize());
        ATH_CHECK(m_jet_selection.retrieve());
        /// Somehow the tau WG grp decided that people should
        /// add AFII systematics by hand
        if (!isData() && m_systematics->isAF2()) {
            for (const auto& sys : {
                     "TAUS_TRUEHADTAU_SME_TES_AFII",
                     "TAUS_TRUEHADTAU_SME_RECO_AFII",
                 }) {
                for (int i : {-1, 1}) {
                    CP::SystematicSet af_ii;
                    af_ii.insert(CP::SystematicVariation(sys, i));
                    ATH_CHECK(m_systematics->InsertKinematicSystematic(af_ii, XAMPP::SelectionObject::Tau));
                }
            }
            for (const auto& sys : {"TAUS_TRUEHADTAU_EFF_RECO_AFII", "TAUS_TRUEHADTAU_EFF_JETID_AFII"}) {
                for (int i : {-1, 1}) {
                    CP::SystematicSet af_ii;
                    af_ii.insert(CP::SystematicVariation(sys, i));
                    ATH_CHECK(m_systematics->InsertWeightSystematic(af_ii, XAMPP::SelectionObject::Tau));
                }
            }
        }
        m_performTauPromotion = m_performTauPromotion && !isData();
        if (m_performTauPromotion) {
            m_rnd_generator = std::make_unique<TRandom3>();
            m_rnd_generator->SetSeed(m_random_seed);
            ClearFromDuplicates(m_promotion_dsids);
            // Remove the false flag to enable the branches
            // in the output
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_antiFakeTaus"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_FakeTaus"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_AllFakeTaus"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("EventIsPromoted"));
            m_NumPoolPromotedTaus = m_XAMPPInfo->GetVariableStorage<int>("N_antiFakeTaus");
            m_NumFakeTaus = m_XAMPPInfo->GetVariableStorage<int>("N_FakeTaus");
            m_NumAllFakeTaus = m_XAMPPInfo->GetVariableStorage<int>("N_AllFakeTaus");
            m_FlagPromotedEvent = m_XAMPPInfo->GetVariableStorage<char>("EventIsPromoted");
        }
        return StatusCode::SUCCESS;
    }
    StatusCode StauTauSelector::TauPromotion(const CP::SystematicSet& systset) {
        // If the current systematic does not affect the taus the nominal container is loaded
        // We've already promoted a tau as signal in the nominal case, we would that a second one gets added, too
        if (!m_performTauPromotion || (m_promotion_before_OR && !SystematicAffects(systset))) return StatusCode::SUCCESS;

        xAOD::TauJetContainer* promotion_container = nullptr;
        xAOD::TauJetContainer* fake_containerTaus = nullptr;
        xAOD::TauJetContainer* fake_AllcontainerTaus = nullptr;
        ATH_CHECK(ViewElementsContainer("promotionCandidates", promotion_container));
        ATH_CHECK(ViewElementsContainer("fakeContainerTaus", fake_containerTaus));
        ATH_CHECK(ViewElementsContainer("fakeAllContainerTaus", fake_AllcontainerTaus));
        for (auto tau : *GetTaus()) {
            if (!PassBaselineKinematics(tau) || !(tau->nTracks() == 1 || tau->nTracks() == 3) || fabs(Charge(tau)) != 1) continue;
            ATH_CHECK(StoreTruthClassifer(*tau));
            dec_isPromoted(*tau) = false;
            if (!isReal(tau)) {
                dec_NTrks(*tau) = tau->nTracks();
                fake_AllcontainerTaus->push_back(tau);
                if (!GetTauDecorations()->passSignal(*tau))
                    promotion_container->push_back(tau);
                else
                    fake_containerTaus->push_back(tau);
            }
        }
        if (m_performFakeEff) {
            for (auto tau : *fake_AllcontainerTaus) {
                if (!IsInContainer(tau, GetBaselineTaus())) {
                    GetBaselineTaus()->push_back(tau);
                    GetBaselineTaus()->sort(XAMPP::ptsorter);
                }
            }
        }

        unsigned int n_promotable = promotion_container->size();

        if (n_promotable > 0 && (m_promotion_dsids.empty() || IsInVector(m_XAMPPInfo->mcChannelNumber(), m_promotion_dsids))) {
            // Promote a random tau to be signal
            xAOD::TauJet* promotedTau = promotion_container->at(m_rnd_generator->Integer(n_promotable));
            SetSelectionDecorators(*promotedTau, true);
            dec_isPromoted(*promotedTau) = true;
            if (m_promotion_before_OR) {
                GetPreTaus()->push_back(promotedTau);
                GetPreTaus()->sort(XAMPP::ptsorter);
            } else {
                ATH_CHECK(StoreTruthClassifer(*promotedTau));
                dec_NTrks(*promotedTau) = promotedTau->nTracks();
                if (!IsInContainer(promotedTau, GetBaselineTaus())) {
                    GetBaselineTaus()->push_back(promotedTau);
                    GetBaselineTaus()->sort(XAMPP::ptsorter);
                }
                GetSignalTaus()->push_back(promotedTau);
                GetSignalTaus()->sort(XAMPP::ptsorter);
            }
        }
        return StatusCode::SUCCESS;
    }
    StatusCode StauTauSelector::InitialFill(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSYTauSelector::InitialFill(systset));
        if (m_promotion_before_OR) ATH_CHECK(TauPromotion(systset));
        return StatusCode::SUCCESS;
    }
    StatusCode StauTauSelector::FillTaus(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSYTauSelector::FillTaus(systset));

        if (!m_promotion_before_OR) ATH_CHECK(TauPromotion(systset));

        // Define a new collection of taus to be piped to the OR. That is essential if we're talking about
        // the correct definition of jets in the event. Baseline taus might remove extra jets, for sure. But if
        // we use only signal taus instead, then the jet is counted twice. However, thus far lep had and had had are only
        // intersted in the OR with the two leading baseline tau candidates.
        xAOD::TauJetContainer* OR_taus = nullptr;
        ATH_CHECK(ViewElementsContainer("TausAfterOR", OR_taus));
        unsigned int NTau = 0;
        for (auto itau : *GetBaselineTaus()) {
            if (NTau >= m_NumTauCandidate_OR) break;
            OR_taus->push_back(itau);
        }

        if (m_performTauPromotion) {
            xAOD::TauJetContainer* promoted_taus = nullptr;
            xAOD::TauJetContainer* fake_containerTaus = nullptr;
            xAOD::TauJetContainer* fake_AllcontainerTaus = nullptr;
            // Load the nominal container from the store gate if the systematic is not affecting the tau
            // kinematics
            ATH_CHECK(LoadViewElementsContainer("promotionCandidates", promoted_taus, !SystematicAffects(systset)));
            ATH_CHECK(LoadViewElementsContainer("fakeContainerTaus", fake_containerTaus, !SystematicAffects(systset)));
            ATH_CHECK(LoadViewElementsContainer("fakeAllContainerTaus", fake_AllcontainerTaus, !SystematicAffects(systset)));
            ATH_CHECK(m_NumPoolPromotedTaus->Store(promoted_taus->size()));
            ATH_CHECK(m_NumFakeTaus->Store(1 + fake_containerTaus->size()));
            ATH_CHECK(m_NumAllFakeTaus->Store(fake_AllcontainerTaus->size()));
            ATH_CHECK(m_FlagPromotedEvent->Store(!promoted_taus->empty()));
        }

        for (auto itau : *GetPreTaus()) {
            /// Track jet width
            const xAOD::Jet* NearJet = dynamic_cast<const xAOD::Jet*>(GetClosestParticle(m_jet_selection->GetPreJets(), itau));  // EM jets
            float Width(-1.), Width_Trk1000(-1.);
            int NTrks_1000 = 0;
            unsigned int idx = m_XAMPPInfo->GetPrimaryVertex() ? m_XAMPPInfo->GetPrimaryVertex()->index() : 0;
            if (NearJet) {
                if (acc_width.isAvailable(*NearJet)) Width = acc_width(*NearJet);
                std::vector<int> nTrkVec;
                std::vector<float> vTrkWidth1000;
                NearJet->getAttribute(xAOD::JetAttribute::TrackWidthPt1000, vTrkWidth1000);

                NTrks_1000 = nTrkVec.size() > idx ? nTrkVec[idx] : -1;
                Width_Trk1000 = vTrkWidth1000.size() > idx ? vTrkWidth1000[idx] : -1;
            }
            dec_width(*itau) = Width;
            dec_trk_width(*itau) = Width_Trk1000;
            dec_NTrksJet(*itau) = NTrks_1000;

            /// Let's see if we can obtain some valuable information from the track
            const xAOD::TauTrack* tau_track = nullptr;
            for (auto& itrack : itau->tracks()) {
                if (!tau_track || tau_track->pt() < itrack->pt()) tau_track = itrack;
            }
            float d0sig = -999;
            float d0 = -999;
            float z0SinTheta = -999;

            if (tau_track) {
                const xAOD::TrackParticle* track = tau_track->track();
                float primvertex_z = m_XAMPPInfo->GetPrimaryVertex() ? m_XAMPPInfo->GetPrimaryVertex()->z() : 0;
                const xAOD::EventInfo* evtInfo = m_XAMPPInfo->GetEventInfo();
                z0SinTheta = (track->z0() + track->vz() - primvertex_z) * TMath::Sin(itau->p4().Theta());
                d0 = track->d0();
                try {
                    d0sig = xAOD::TrackingHelpers::d0significance(track, evtInfo->beamPosSigmaX(), evtInfo->beamPosSigmaY(),
                                                                  evtInfo->beamPosSigmaXY());
                } catch (...) { d0sig = -999; }
            }

            dec_d0sig(*itau) = d0sig;
            dec_z0sinTheta(*itau) = z0SinTheta;
            dec_d0(*itau) = d0;

            int Quality = 0;
            if (itau->isTau(xAOD::TauJetParameters::JetRNNSigTight))
                Quality = 3;
            else if (itau->isTau(xAOD::TauJetParameters::JetRNNSigMedium))
                Quality = 2;
            else if (itau->isTau(xAOD::TauJetParameters::JetRNNSigLoose))
                Quality = 1;

            dec_quality(*itau) = Quality;
        }
        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
