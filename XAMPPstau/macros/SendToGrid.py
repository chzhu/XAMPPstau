#!/env/python
import os, sys
from ClusterSubmission.Utils import ResolvePath, WriteList, id_generator
from XAMPPstau.SubmitToGrid import getLogicalDataSets
import getpass

PROD = "V27"
USER = "jojungge"
UPDATE = False
SEND = False
MONITOR = True

Failed = [x for x in getLogicalDataSets().iterkeys() if x.startswith("StauStau") or x.find("Ztautau") != -1]
Failed = [
    #        "PowHegPy8_Zee",
    #        "PowHegPy8_Zmumu",
    #        "PowHegPy8_Ztautau",
    #
    #        "PowHegPy8_Wenu",
    #        "PowHegPy8_Wmunu",
    #        "PowHegPy8_Wtaunu",
    "Sherpa221_Wmunu",
    "Sherpa222_VV",
    "StauStau_120_40",
    "StauStau_160_40",
    "StauStau_240_160",
    "StauStau_240_40",
    "StauStau_300_40",
    "StauStau_320_40",
    "StauStau_380_1",
    "StauStau_400_120",
    "StauStau_400_40",
    "StauStau_440_1",
    "Wh_hall_152p5_22p5_1T",
    "Wh_hall_275p0_75p0_1T",
    "Wh_hall_300p0_50p0_1T",
    "Wh_hall_375p0_50p0_1T",
    "Wh_hall_400p0_0p0_1T",
]
#update

Failed = [
    #"PowHegPy8_ttbar_MET_sliced",
    #"MG5_aMCatNLO_Py8_ttV_tV_tVV",
    #"PowHegPy8_single_top_Wt_chan_incl",
    #"PowHegPy8_Wenu"                    ,
    #"Sherpa221_Wenu",
]  #"Wh_hall_175p0_25p0_1T"]

SampleDir = ResolvePath("XAMPPstau/SampleLists/mc16_13TeV/lephad/")
Update_Cmd = "python %s --ListDir %s --derivation SUSY18" % (ResolvePath("XAMPPbase/python/UpdateSampleLists.py"), SampleDir)
if UPDATE: os.system(Update_Cmd)

#submit
Cmd = "python %s --stream LepHad --sampleList %s --production %s %s --productionRole det-muon --noSyst" % (
    ResolvePath("XAMPPstau/python/SubmitToGrid.py"), " ".join(
        ["%s/%s" % (SampleDir, S)
         for S in os.listdir(SampleDir) if S.find("SUSY18_MC16_SIG") == -1]), PROD, "" if len(Failed) == 0 else "--physicalSamples %s" %
    (" ".join(Failed)))
if SEND: os.system(Cmd)

#monitor
Monitor_Cmd = "python %s --stream LepHad --production %s --replicate_ds --destRSE MPPMU_LOCALGROUPDISK --rucio %s --productionRole det-muon --automatic_retry --change_files_per_job 1 --noSyst" % (
    ResolvePath("XAMPPstau/python/checkProduction.py"), PROD, USER)
if MONITOR: os.system(Monitor_Cmd)
