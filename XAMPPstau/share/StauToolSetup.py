include("XAMPPbase/BaseToolSetup.py")


def SetupStauTruthAnalysisConfig(name="TruthStaus"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisConfig"):
        from XAMPPstau.XAMPPstauConf import XAMPP__StauTruthAnalysisConfig
        AnaConfig = CfgMgr.XAMPP__StauTruthAnalysisConfig(name="AnalysisConfig", TreeName=name)
        ToolSvc += AnaConfig

    return getattr(ToolSvc, "AnalysisConfig")


def SetupTruthStauAnaHelper(TreeName="TruthStaus"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisHelper"):
        from XAMPPstau.XAMPPstauConf import XAMPP__StauTruthAnalysisHelper
        BaseHelper = CfgMgr.XAMPP__StauTruthAnalysisHelper("AnalysisHelper")
        BaseHelper.AnalysisConfig = SetupStauTruthAnalysisConfig(TreeName)
        BaseHelper.SystematicsTool = SetupSystematicsTool(noJets=True,
                                                          noBtag=True,
                                                          noElectrons=True,
                                                          noMuons=True,
                                                          noTaus=True,
                                                          noDiTaus=True,
                                                          noPhotons=True)
        BaseHelper.TruthSelector = SetupTruthSelector()
        ToolSvc += BaseHelper
    return getattr(ToolSvc, "AnalysisHelper")


def SetupStauTauSelector():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SUSYTauSelector"):
        from XAMPPstau.XAMPPstauConf import XAMPP__StauTauSelector
        TauSelector = CfgMgr.XAMPP__StauTauSelector("SUSYTauSelector")
        TauSelector.JetSelectionTool = SetupSUSYJetSelector()
        from TauAnalysisTools.TauAnalysisToolsConf import TauAnalysisTools__TauSelectionTool
        from ClusterSubmission.Utils import ResolvePath
        TauSelTool = CfgMgr.TauAnalysisTools__TauSelectionTool("TriggerBDTTauSelection")
        TauSelTool.ConfigPath = ResolvePath("XAMPPstau/SignalTauId.conf")
        ToolSvc += TauSelTool
        TauSelector.TriggerSignalTauSelectionTool = TauSelTool

        TauSelTool = CfgMgr.TauAnalysisTools__TauSelectionTool("TriggerBDTBaseTauSelection")
        TauSelTool.ConfigPath = ResolvePath("XAMPPstau/BaseTauId.conf")
        ToolSvc += TauSelTool
        TauSelector.TriggerBaselineTauSelectionTool = TauSelTool

        ToolSvc += TauSelector
    return getattr(ToolSvc, "SUSYTauSelector")


def SetupStauTriggerTool():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "TriggerTool"):
        from XAMPPstau.XAMPPstauConf import XAMPP__StauTriggerTool
        TriggerTool = CfgMgr.XAMPP__StauTriggerTool("TriggerTool")
        TriggerTool.SystematicsTool = SetupSystematicsTool()
        TriggerTool.ElectronSelector = SetupSUSYElectronSelector()
        TriggerTool.MuonSelector = SetupSUSYMuonSelector()
        # TriggerTool.JetSelector = SetupSUSYJetSelector()
        TriggerTool.TauSelector = SetupStauTauSelector()
        TriggerTool.PhotonSelector = SetupSUSYPhotonSelector()
        ToolSvc += TriggerTool
    return getattr(ToolSvc, "TriggerTool")


def SetupLepHadStauConfig(name="Staus"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisConfig"):
        from XAMPPstau.XAMPPstauConf import XAMPP__StauLepHadAnalysisConfig
        AnaConfig = CfgMgr.XAMPP__StauLepHadAnalysisConfig(name="AnalysisConfig", TreeName=name)
        ToolSvc += AnaConfig

    return getattr(ToolSvc, "AnalysisConfig")


def SetupHadHadStauConfig(name="Staus"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisConfig"):
        from XAMPPstau.XAMPPstauConf import XAMPP__StauHadHadAnalysisConfig
        AnaConfig = CfgMgr.XAMPP__StauHadHadAnalysisConfig(name="AnalysisConfig", TreeName=name)
        ToolSvc += AnaConfig

    return getattr(ToolSvc, "AnalysisConfig")


def SetupStauAnaHelper(TreeName="Staus", InitLepHadChannel=True):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisHelper"):
        from XAMPPstau.XAMPPstauConf import XAMPP__StauAnalysisHelper
        BaseHelper = CfgMgr.XAMPP__StauAnalysisHelper("AnalysisHelper")
        BaseHelper.AnalysisConfig = SetupLepHadStauConfig(TreeName) if InitLepHadChannel else SetupHadHadStauConfig(TreeName)
        BaseHelper.SystematicsTool = SetupSystematicsTool(noJets=False,
                                                          noBtag=False,
                                                          noElectrons=False,
                                                          noMuons=False,
                                                          noTaus=False,
                                                          noDiTaus=True,
                                                          noPhotons=True)
        BaseHelper.TauSelector = SetupStauTauSelector()

        ToolSvc += BaseHelper
    return getattr(ToolSvc, "AnalysisHelper")


def ParseCommonStauOptions(STFile,
                           LepHadStream=True,
                           doEventShapes=False,
                           writeBaselineSF=False,
                           doMetSignificance=False,
                           includeTausInMet=False,
                           doMetSignificnace=False,
                           doTauPromotion=False,
                           useMC16d=True,
                           use17Data=True,
                           use18Data=True):

    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    from XAMPPbase.Utils import GetPropertyFromConfFile
    ##################################################
    #       Ensure truth object matching
    ##################################################
    SetupSUSYElectronSelector().StoreTruthClassifier = True
    SetupSUSYMuonSelector().StoreTruthClassifier = True
    SetupStauTauSelector().StoreTruthClassifier = True
    ###################################################
    #  SUSYTools does no longer calculate the         #
    #  isolation if its requirement is disabled       #
    #  in the config file                             #
    ###################################################
    SetupSUSYMuonSelector().RecalcIsolation = True
    SetupSUSYMuonSelector().RequireIsoSignal = True

    SetupSUSYElectronSelector().RecalcIsolation = True
    SetupSUSYElectronSelector().RequireIsoSignal = True

    ####################################################
    #       Write baseline sfs of electrons and muons
    ####################################################
    SetupStauTauSelector().WriteBaselineSF = writeBaselineSF

    #jets
    SetupSUSYJetSelector().SeparateSF = False
    SetupSUSYJetSelector().ApplyBTagSF = True

    ##############################################################################
    #    make sure that the trigger SFs are excluded from MuoWeight/EleWeight    #
    ##############################################################################
    SetupSUSYElectronSelector().ExcludeTrigSFfromTotalWeight = True
    SetupSUSYMuonSelector().ExcludeTrigSFfromTotalWeight = True

    ################################################
    #          MT2 variable
    ################################################
    from XAMPPstau.XAMPPstauConf import XAMPP__StauMT2Module
    MT2Module = CfgMgr.XAMPP__StauMT2Module("MT2Variables")
    MT2Module.ElectronContainer = "signal_electrons"
    MT2Module.MuonContainer = "signal_muons"
    MT2Module.JetContainer = "signal_jets"
    MT2Module.MissingEt = "MetTST"

    ################################################
    #                Meff module                   #
    ################################################
    from XAMPPstau.XAMPPstauConf import XAMPP__StauMeffModule
    MeffModule = CfgMgr.XAMPP__StauMeffModule("MeffVariables")
    MeffModule.ElectronContainer = "signal_electrons"
    MeffModule.MuonContainer = "signal_muons"
    MeffModule.TauContainer = "signal_taus"
    MeffModule.JetContainer = "signal_jets"
    MeffModule.MissingEt = "MetTST"

    ToolSvc += MT2Module
    ToolSvc += MeffModule
    SetupStauAnaHelper().AnalysisModules += [MT2Module, MeffModule]

    if LepHadStream:
        MT2Module.TauContainer = "signal_leptons"
        MeffModule.TauContainer = "candidate_taus"
        MeffModule.LeptonContainer = "signal_leptons"
        SetupStauTauSelector().TauCandidatesForOR = 1
    else:
        MT2Module.TauContainer = "candidate_taus"  ## The Tau Container is the container used by the StauMt2Module
        ### The lepton container is used to calculate the Bisect/ sum cos delpha / effective pt variable
        MT2Module = CfgMgr.XAMPP__StauMT2Module("MT2Variables_emt")
        MT2Module.ElectronContainer = "signal_electrons"
        MT2Module.MuonContainer = "signal_muons"
        MT2Module.JetContainer = "signal_jets"
        MT2Module.TauContainer = "signal_leptons"
        MT2Module.GroupName = "emt"
        MT2Module.MissingEt = "MetTST"
        ToolSvc += MT2Module
        SetupStauAnaHelper().AnalysisModules += [MT2Module]

        MeffModule.LeptonContainer = "candidate_taus"
        MeffModule.SaveHadHad = True
        SetupStauTauSelector().TauCandidatesForOR = 1000

    ################################################
    #          MCT variable
    ################################################
    from XAMPPstau.XAMPPstauConf import XAMPP__StauMCTModule
    MCTModule = CfgMgr.XAMPP__StauMCTModule("MCTVariables")
    MCTModule.ElectronContainer = "signal_electrons"
    MCTModule.TauContainer = "candidate_taus"
    MCTModule.MuonContainer = "signal_muons"
    MCTModule.JetContainer = "signal_jets"
    MCTModule.MissingEt = "MetTST"

    ToolSvc += MCTModule
    SetupStauAnaHelper().AnalysisModules += [MCTModule]

    ############################################
    #    Enable the event shape variables
    ############################################
    if doEventShapes:
        from XAMPPstau.XAMPPstauConf import XAMPP__EventShapeModule
        Module = CfgMgr.XAMPP__EventShapeModule("EventShapeVariables")
        Module.CalculateSphericity = True
        Module.CalculateSpherocity = True
        Module.CalculateThrust = True
        Module.CalculateFoxWolfram = True
        Module.NormalizeWolframMoments = True
        Module.OrderWolframMoments = 12

        Module.ElectronContainer = "signal_electrons"
        Module.MuonContainer = "signal_muons"
        Module.TauContainer = "candidate_taus"
        Module.JetContainer = "signal_jets"
        Module.MissingEt = "MetTST"

        ToolSvc += Module
        SetupStauAnaHelper().AnalysisModules += [Module]

    SetupSUSYMetSelector().WriteMetSignificance = doMetSignificance
    SetupSUSYMetSelector().IncludeTaus = includeTausInMet
    ###################################################
    #       Setup the Recursive JigSaw algorithms     #
    ###################################################
    from XAMPPstau.XAMPPstauConf import XAMPP__StauJigSawZModule
    JigSawModuleZ = CfgMgr.XAMPP__StauJigSawZModule("ZJigSaw")
    JigSawModuleZ.ElectronContainer = "signal_electrons"
    JigSawModuleZ.MuonContainer = "signal_muons"
    JigSawModuleZ.TauContainer = "candidate_taus"
    JigSawModuleZ.JetContainer = "signal_jets"
    JigSawModuleZ.MissingEt = "MetTST"
    JigSawModuleZ.GroupName = "RJZ"

    ToolSvc += JigSawModuleZ
    SetupStauAnaHelper().AnalysisModules += [JigSawModuleZ]

    from XAMPPstau.XAMPPstauConf import XAMPP__StauJigSawWModule
    JigSawModuleW = CfgMgr.XAMPP__StauJigSawWModule("WJigSaw")
    JigSawModuleW.ElectronContainer = "signal_electrons"
    JigSawModuleW.MuonContainer = "signal_muons"
    JigSawModuleW.TauContainer = "candidate_taus"
    JigSawModuleW.JetContainer = "signal_jets"
    JigSawModuleW.MissingEt = "MetTST"
    JigSawModuleW.GroupName = "RJW"
    ToolSvc += JigSawModuleW
    SetupStauAnaHelper().AnalysisModules += [JigSawModuleW]
    ###########################################################
    #       Tau promotion to enhance the statistics in one    #
    #       fake backgrounds for had had mainly.              #
    ###########################################################
    if doTauPromotion or "runWithTauPromotion" in globals():
        SetupStauAnaHelper().doTauPromotion = True
        SetupStauTauSelector().doTauPromotion = True
        SetupStauTauSelector().BaselinePtCut = 20.e3
        SetupStauTauSelector().BaselineEtaCut = 2.5
        SetupStauTauSelector().ExcludeBaselineEta = ["-1.52;-1.37", "1.37;1.52"]
        SetupStauTauSelector().PromotionSeed = 9752984
        ### Restrict the tau  promotion only  to  W+jets samples
        #from XAMPPstau.SubmitToGrid import getLogicalDataSets
        #SetupStauTauSelector().doPromotionOnlyFor += getLogicalDataSets()["PowHegPy8_Wtaunu"] + getLogicalDataSets()["Sherpa221_Wtaunu"]

    ParseBasicConfigsToHelper(STFile=STFile, SeparateSF=False)

    recoLog = logging.getLogger('XAMPP StauTool setup')
    ###############################################################################################
    ##      The BTagging people broke the naming schme of their containers
    ##  https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagTaggerRecommendationsRelease21
    ##  https://indico.cern.ch/event/836130/contributions/3504999/attachments/1884841/3106548/2019-07-23-FTAGPlenary.pdf
    ###############################################################################################
    try:
        p_tag = int(getFlags().p_tag()[1:])
    except:
        recoLog.warning("Could not derive the p-tag")
        p_tag = -1

    if p_tag >= 3954:
        SetupSUSYJetSelector().AntiKt4BTagContainer = "AntiKt4EMTopoJets_BTagging201810"

    try:
        JetType = int(GetPropertyFromConfFile(STFile, "Jet.InputType"))
    except:
        recoLog.warning("Could not find the Jet.InputType")

    if JetType == 1:
        JetCollectionName = "AntiKt4EMTopoJets"
    elif JetType == 9:
        JetCollectionName = "AntiKt4EMPFlowJets"
        # Only when we use PFlow jets do the DFCommonCrackVeto
        SetupHadHadStauConfig().doPFlowCleaning = True
        SetupStauAnaHelper().doPFlowCleaning = True
        PFlow_JVT_WP = GetPropertyFromConfFile(STFile, "Jet.JVT_WP")
        if not PFlow_JVT_WP == "Tight":
            print "ERROR: Selected %s JVT WP for PFlow jets! You should use tight WP instead." % (PFlow_JVT_WP)
            exit(1)
    else:
        print "WARNING: Unknown jet collection for small-R jets: Jet.InputType = {}".format(JetType)
        exit(1)
