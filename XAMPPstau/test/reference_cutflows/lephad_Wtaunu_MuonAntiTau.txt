Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto               DSID_361102_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    3627 ± 60.22
PassGRL                    3627 ± 60.22
passLArTile                3627 ± 60.22
Trigger                    3624 ± 60.20
HasVtx                     3624 ± 60.20
BadJet                     3620 ± 60.17
CosmicMuon                 3608 ± 60.07
BadMuon                    3608 ± 60.07
TrigMatching               2939 ± 54.21
N_{#mu}^{baseline} #geq 1  788  ± 28.07
N_{e}^{signal} = 0         787  ± 28.05
N_{#tau}^{baseline}>=1     169  ± 13.00
E_{T}^{miss} > 15 GeV      150  ± 12.25
N_{#tau}^{signal}==0       143  ± 11.96
Final                      143  ± 11.96
##############################################
Printing raw events
CutFlowHisto               DSID_361105_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    3835 ± 61.93
PassGRL                    3835 ± 61.93
passLArTile                3835 ± 61.93
Trigger                    3831 ± 61.90
HasVtx                     3831 ± 61.90
BadJet                     3826 ± 61.85
CosmicMuon                 3816 ± 61.77
BadMuon                    3816 ± 61.77
TrigMatching               3166 ± 56.27
N_{#mu}^{baseline} #geq 1  896  ± 29.93
N_{e}^{signal} = 0         896  ± 29.93
N_{#tau}^{baseline}>=1     205  ± 14.32
E_{T}^{miss} > 15 GeV      172  ± 13.11
N_{#tau}^{signal}==0       165  ± 12.85
Final                      165  ± 12.85
##############################################
