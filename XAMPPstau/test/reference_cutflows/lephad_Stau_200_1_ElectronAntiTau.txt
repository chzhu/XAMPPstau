Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto             DSID_396104_CutFlow
Systematic variation     Nominal   
############################################
Cut                      Yields    
############################################
Initial                  10782 ± 103.84
PassGRL                  10782 ± 103.84
passLArTile              10782 ± 103.84
Trigger                  10762 ± 103.74
HasVtx                   10762 ± 103.74
BadJet                   10748 ± 103.67
CosmicMuon               10511 ± 102.52
BadMuon                  10511 ± 102.52
TrigMatching             9723  ± 98.61
N_{e}^{baseline} #geq 1  5535  ± 74.40
N_{#mu}^{signal} = 0     4928  ± 70.20
N_{#tau}^{baseline}>=1   2898  ± 53.83
E_{T}^{miss} > 15 GeV    2840  ± 53.29
N_{#tau}^{signal}==0     1144  ± 33.82
Final                    1144  ± 33.82
############################################
Printing raw events
CutFlowHisto             DSID_396104_CutFlow
Systematic variation     Nominal   
############################################
Cut                      Yields    
############################################
Initial                  10782 ± 103.84
PassGRL                  10782 ± 103.84
passLArTile              10782 ± 103.84
Trigger                  10762 ± 103.74
HasVtx                   10762 ± 103.74
BadJet                   10748 ± 103.67
CosmicMuon               10511 ± 102.52
BadMuon                  10511 ± 102.52
TrigMatching             9723  ± 98.61
N_{e}^{baseline} #geq 1  5535  ± 74.40
N_{#mu}^{signal} = 0     4928  ± 70.20
N_{#tau}^{baseline}>=1   2898  ± 53.83
E_{T}^{miss} > 15 GeV    2840  ± 53.29
N_{#tau}^{signal}==0     1144  ± 33.82
Final                    1144  ± 33.82
############################################
Printing raw events
CutFlowHisto             DSID_396104_CutFlow
Systematic variation     Nominal   
############################################
Cut                      Yields    
############################################
Initial                  10782 ± 103.84
PassGRL                  10782 ± 103.84
passLArTile              10782 ± 103.84
Trigger                  10762 ± 103.74
HasVtx                   10762 ± 103.74
BadJet                   10748 ± 103.67
CosmicMuon               10511 ± 102.52
BadMuon                  10511 ± 102.52
TrigMatching             9723  ± 98.61
N_{e}^{baseline} #geq 1  5535  ± 74.40
N_{#mu}^{signal} = 0     4928  ± 70.20
N_{#tau}^{baseline}>=1   2898  ± 53.83
E_{T}^{miss} > 15 GeV    2840  ± 53.29
N_{#tau}^{signal}==0     1144  ± 33.82
Final                    1144  ± 33.82
############################################
