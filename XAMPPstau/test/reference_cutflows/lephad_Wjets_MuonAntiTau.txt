Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto               DSID_361100_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    139316 ± 373.25
PassGRL                    139316 ± 373.25
passLArTile                139316 ± 373.25
Trigger                    139285 ± 373.21
HasVtx                     139285 ± 373.21
BadJet                     139051 ± 372.90
CosmicMuon                 139051 ± 372.90
BadMuon                    139051 ± 372.90
TrigMatching               126399 ± 355.53
N_{#mu}^{baseline} #geq 1  12     ± 3.46
##############################################
Printing raw events
CutFlowHisto               DSID_361101_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    65570 ± 256.07
PassGRL                    65570 ± 256.07
passLArTile                65570 ± 256.07
Trigger                    65562 ± 256.05
HasVtx                     65562 ± 256.05
BadJet                     65428 ± 255.79
CosmicMuon                 65400 ± 255.73
BadMuon                    65397 ± 255.73
TrigMatching               57853 ± 240.53
N_{#mu}^{baseline} #geq 1  57784 ± 240.38
N_{e}^{signal} = 0         57765 ± 240.34
N_{#tau}^{baseline}>=1     9438  ± 97.15
E_{T}^{miss} > 15 GeV      8675  ± 93.14
N_{#tau}^{signal}==0       8243  ± 90.79
Final                      8243  ± 90.79
##############################################
Printing raw events
CutFlowHisto               DSID_361103_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    215769 ± 464.51
PassGRL                    215769 ± 464.51
passLArTile                215769 ± 464.51
Trigger                    215688 ± 464.42
HasVtx                     215687 ± 464.42
BadJet                     215296 ± 464.00
CosmicMuon                 215294 ± 464.00
BadMuon                    215294 ± 464.00
TrigMatching               196710 ± 443.52
N_{#mu}^{baseline} #geq 1  24     ± 4.90
N_{e}^{signal} = 0         2      ± 1.41
##############################################
Printing raw events
CutFlowHisto               DSID_361104_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    66538 ± 257.95
PassGRL                    66538 ± 257.95
passLArTile                66538 ± 257.95
Trigger                    66536 ± 257.95
HasVtx                     66536 ± 257.95
BadJet                     66382 ± 257.65
CosmicMuon                 66363 ± 257.61
BadMuon                    66358 ± 257.60
TrigMatching               59812 ± 244.56
N_{#mu}^{baseline} #geq 1  59749 ± 244.44
N_{e}^{signal} = 0         59718 ± 244.37
N_{#tau}^{baseline}>=1     9998  ± 99.99
E_{T}^{miss} > 15 GeV      9231  ± 96.08
N_{#tau}^{signal}==0       8827  ± 93.95
Final                      8827  ± 93.95
##############################################
