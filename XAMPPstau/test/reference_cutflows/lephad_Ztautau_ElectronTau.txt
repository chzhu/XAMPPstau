Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto             DSID_361108_CutFlow
Systematic variation     Nominal   
############################################
Cut                      Yields    
############################################
Initial                  17494 ± 132.26
PassGRL                  17494 ± 132.26
passLArTile              17494 ± 132.26
Trigger                  17473 ± 132.19
HasVtx                   17473 ± 132.19
BadJet                   17409 ± 131.94
CosmicMuon               17277 ± 131.44
BadMuon                  17277 ± 131.44
TrigMatching             14503 ± 120.43
N_{e}^{baseline} #geq 1  7950  ± 89.16
N_{#mu}^{signal} = 0     7318  ± 85.55
N_{#tau}^{baseline}>=1   2248  ± 47.41
E_{T}^{miss} > 15 GeV    1365  ± 36.95
N_{#tau}^{signal}==1     593   ± 24.35
Final                    593   ± 24.35
############################################
