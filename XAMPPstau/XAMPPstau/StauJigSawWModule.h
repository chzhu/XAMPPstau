#ifndef StauJigSawWModule_H
#define StauJigSawWModule_H

#include <XAMPPstau/StauJigSawZModule.h>

namespace XAMPP {
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    //  Recusrive Jigsaw reconstruction, described in https://arxiv.org/abs/1705.10733
    //  This module implements a RJ aiming to reconstruct the W from W+jets.
    //  It's more or less copy pasted from the introductionary example from the Recursive JigSaw sample

    //  The basic idea to choose this simple approach is such that W+jets events are more likely to have
    //   a leptonically decaying (i.e. e/mu) vector boson. One of the jets is misidentified as tau to form a lep-had event
    class StauJigSawWModule : public StauJigSawZModule {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(StauJigSawWModule, XAMPP::IAnalysisModule)
        //
        StauJigSawWModule(const std::string& myname);
        virtual ~StauJigSawWModule();

        virtual StatusCode fill();

        virtual unsigned int flavour() const;
        virtual StatusCode bookVariables();

    private:
        virtual RecoFrame_Ptr getRestFrame(int frame) const;

        virtual StatusCode setupRJanalysis();
        VisibleRecoFrame_Ptr m_lep;
        RecoFrame_Ptr m_nu;
        RecoFrame_Ptr m_W;

        XAMPP::Storage<float>* m_dec_cosThetaStar;
        XAMPP::Storage<float>* m_dec_dPhi;
        XAMPP::Storage<float>* m_dec_GammaBeta;
    };
}  // namespace XAMPP
#endif
