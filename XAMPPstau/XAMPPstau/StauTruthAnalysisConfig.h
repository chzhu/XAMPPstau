#ifndef XAMPPStau_StauTruthAnalysisConfig_h
#define XAMPPStau_StauTruthAnalysisConfig_h
#include <XAMPPbase/AnalysisConfig.h>

namespace XAMPP {
    class StauTruthAnalysisConfig : public TruthAnalysisConfig {
    public:
        ASG_TOOL_CLASS(StauTruthAnalysisConfig, XAMPP::IAnalysisConfig)
        StauTruthAnalysisConfig(const std::string& Analysis = "TruthStaus");
        virtual ~StauTruthAnalysisConfig() {}

    protected:
        virtual StatusCode initializeCustomCuts();
    };

}  // namespace XAMPP
#endif
