#ifndef XAMPPstau_StauAnalysisUtils_H
#define XAMPPstau_StauAnalysisUtils_H
#include <XAMPPbase/AnalysisUtils.h>

#include <fstream>
#include <iostream>
#include <sstream>

namespace XAMPP {

    bool ZMassSorter(const xAOD::IParticle* p1, const xAOD::IParticle* p2);

    /*! boost*/
    TVector3 GetRestBoost(const xAOD::IParticle& Part);
    TVector3 GetRestBoost(const xAOD::IParticle* Part);
    TVector3 GetRestBoost(TLorentzVector Vec);

    float VecSumPt(const xAOD::IParticle* v1, const xAOD::IParticle* v2);

    float SumCosDeltaPhi(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met);
    float CosMinDeltaPhi(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met);

    double METcentrality(const xAOD::IParticle* p1, const xAOD::IParticle* p2, XAMPP::Storage<xAOD::MissingET*>* met);
    double METcentrality(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met);

    float PairMetDeltaPhi(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met);

    float PTt(const xAOD::IParticle* p1, const xAOD::IParticle* p2);

    float CosChi1(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::IParticle* p3);
    float CosChi2(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::IParticle* p3, const xAOD::IParticle* p4);

    double calc_mct(const xAOD::IParticle* p1, const xAOD::IParticle* p2);

    bool toptag0j(const xAOD::IParticle* p1, const xAOD::IParticle* p2, XAMPP::Storage<xAOD::MissingET*>* met);
    bool toptag0j(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met);

    unsigned int getNumTopPairs(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::JetContainer* jets,
                                const xAOD::MissingET* met, unsigned int use_n_jets = -1);

    bool isReal(const xAOD::IParticle* particle);
    bool isReal(const xAOD::IParticle& particle);

    bool METbisect(const xAOD::IParticle* p1, const xAOD::IParticle* p2, XAMPP::Storage<xAOD::MissingET*>* met);
    bool METbisect(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met);

    float PtEffective(const xAOD::IParticle* p1, const xAOD::IParticle* p2, XAMPP::Storage<xAOD::MissingET*>* met);
    float PtEffective(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met);

    void CopyContainer(xAOD::IParticleContainer* From, xAOD::IParticleContainer* To);
    void CopyContainer(xAOD::IParticleContainer* From, xAOD::IParticleContainer& To);
    void CopyContainer(xAOD::IParticleContainer* From, xAOD::IParticleContainer& To);
    void CopyContainer(xAOD::IParticleContainer& From, xAOD::IParticleContainer& To);

    TLorentzVector getOriginalP4(const xAOD::TruthParticle* p);

}  // namespace XAMPP
#endif
