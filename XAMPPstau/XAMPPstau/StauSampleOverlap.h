#ifndef StauSampleOverlap_H
#define StauSampleOverlap_H

#include <AsgTools/AnaToolHandle.h>
#include <AsgTools/AsgTool.h>
#include <AsgTools/ToolHandle.h>
#include <XAMPPbase/EventInfo.h>
#include <XAMPPbase/ISystematics.h>

namespace XAMPP {
    class ISampleOverlap : virtual public asg::IAsgTool {
        ASG_TOOL_INTERFACE(XAMPP::ISampleOverlap)
    public:
        virtual StatusCode initialize() = 0;
        virtual bool AcceptEvent() const = 0;
        virtual ~ISampleOverlap() = default;
    };

    class StauSampleOverlap : public asg::AsgTool, virtual public ISampleOverlap {
    public:
        StauSampleOverlap(const std::string& myname);

        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(StauSampleOverlap, XAMPP::ISampleOverlap)

        virtual ~StauSampleOverlap() = default;
        virtual StatusCode initialize();
        virtual bool AcceptEvent() const;

    private:
        asg::AnaToolHandle<IEventInfo> m_InfoHandle;
        ToolHandle<ISystematics> m_systematics;
        XAMPP::EventInfo* m_XAMPPInfo;

        Storage<float>* m_dec_gen_filt_met;
        Storage<float>* m_dec_gen_filt_ht;
        Storage<double>* m_dec_gen_weight_merge;
    };

}  // namespace XAMPP

#endif
